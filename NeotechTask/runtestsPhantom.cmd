@pushd %~dp0

%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe "NeotechTask.csproj"

@if ERRORLEVEL 1 goto end

@cd ..\packages\SpecRun.Runner.*\tools

@set profile=%1
@if "%profile%" == "" set profile=Default

SpecRun.exe run %profile%.srprofile "/baseFolder:%~dp0\bin\Debug" /logFile:TestResults\TestResults.log /reportFile:TestResults\TestResults.html

if errorlevel 200 exit /b %%errorlevel%%
exit /b 0

:end

@popd
