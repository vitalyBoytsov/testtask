﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeotechTask
{
    public class Config
    {
        public static string Browser => GetAppSetting<string>("Browser");
        public static string CinamonUrl => GetAppSetting<string>("CinamonUrl");
        public static int PageLoadWaitTime => GetAppSetting<int>("PageLoadWaitTime");
        public static int ExplicitWaitTime => GetAppSetting<int>("ExplicitWaitTime");
        public static int ProgressBarDisappearWaitTime => GetAppSetting<int>("ProgressBarDisappearWaitTime");
        public static int ProgressBarAppearWaitTime => GetAppSetting<int>("ProgressBarAppearWaitTime");
        public static string SessionAPI => GetAppSetting<string>("SessionAPI");

        private static T GetAppSetting<T>(string setting)
        {
            try
            {
                return (T)Convert.ChangeType(GetValue(setting), typeof(T));
            }
            catch
            {
                return default(T);
            }
        }

        private static string GetValue(string key)
        {
            try
            {
                return System.Configuration.ConfigurationManager.AppSettings[key];
            }
            catch
            {
                return string.Empty;
            }
        }
    }

     



}
