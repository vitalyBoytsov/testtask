﻿using NeotechTask.Common;
using NeotechTask.Locators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class TicketSelectPageSteps:StepBase
    {
  
        [Given(@"I have selected (.*) of Friends tickets")]
        public void SelectTickets(string amount)
        {
            Action.MultipleClickElement(TicketPageLocators.TicketIncreaseBtn,amount);
        }

        [Given(@"I have pressed \[Next] button in ticket page")]
        [When(@"I press \[Next] button in ticket page")]
        public void NextBtnPress()
        {
            Action.JavaScriptClick(TicketPageLocators.NextBtn);
        }

        [Given(@"I have entered random coupon id")]
        public void EnterRandomCoupon()
        {
            Action.TypeRandomValue(TicketPageLocators.CouponInput);
        }

        [Then(@"Coupon validtion error should be displayed")]
        public void CouponValidation()
        {
            Assert.AreEqual(TestData.CouponError,Text.GetText(TicketPageLocators.Error));
        }

        [When(@"I press \[Submit coupon] button")]
        public void SubmitCouponBtnPress()
        {
            Action.JavaScriptClick(TicketPageLocators.SubmitCoupon);
        } 

    }
}
