﻿using NeotechTask.Common;
using NeotechTask.Locators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class CommonSteps:StepBase
    {
        private static MovieDateTimeObject _movie;
        private ScenarioContext ScenarioContext;

        protected CommonSteps(ScenarioContext scenarioContext)
        {
            ScenarioContext = scenarioContext;
        } 

        [Given(@"I have selected (.*) cinema")]
        public void SelectCinema(string cinemaName)
        {
            Action.SelectValueFromDropDown(CommonLocators.QuickCinemaDrpdwn, cinemaName);
        }

        [Given(@"I have selected random movie that has session after (.*) days")]
        public void RandomMovieSelect(string incr)
        {
            _movie = MovieSessions.GetSpecificDateRandomMovie(CommonLocators.Movies, incr);
            Action.SelectValueFromDropDownByValue(CommonLocators.QuickFilmDrpdwn,_movie.Movie);
        }

        [Given(@"I have selected random movie date")]
        public void RandomSessionSelect()
        {
            Action.SelectValueFromDropDownByValue(CommonLocators.QuickSessionDrpdwn,
                Action.GetElementsValue(MovieSessions.GetSessionIWebEl(CommonLocators.SessionTimes, _movie)));
        }

        [Given(@"I have pressed \[Buy now] button")]
        public void BuyButtonPress()
        {
            Action.Click(CommonLocators.QuickBuyButton);
        }

        [Given(@"I have saved total price")]
        public void SavedTotalPrice()
        {
            ScenarioContext["TotalPrice"] = Text.GetText(CommonLocators.TotalPrice);
        }

        [Given(@"I have checked that we are transfered to (.*) view")]
        public void ProfileViewCheck(string title)
        {
            Assert.IsTrue(Check.CheckWindowTitle(title));
        }
    }
}
