﻿using System.Collections.Generic;
using NeotechTask.Common;
using NeotechTask.Locators;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class SeatsPageSteps: StepBase
    {
        public static List<string> selectedSeats;
     
        [Given(@"I have pressed \[Select] button in seats page")]
        [When(@"I press \[Select] button in seats page")]
        public void GivenIHaveEnteredSomethingIntoTheCalculator()
        {
            Action.JavaScriptClick(SeatsPageLocators.SelectBtn);
        }

        [Given(@"I have selected random seat from (.*) row")]
        public void SelectFifthRowSeat(string row)
        {
            SeatSelector.SelectRandomRowSeats(SeatsPageLocators.Seats, row); 
        }

        [Given(@"I have saved selected seats in seats page")]
        public void SaveSeats()
        {
            selectedSeats = SeatSelector.GetSeatPageSeats(SeatsPageLocators.SelectedSeats);
        }
    }
}
