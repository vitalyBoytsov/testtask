﻿using NeotechTask.Common;
using NeotechTask.Locators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class ConfirmationPageSteps:StepBase
    {
        private ScenarioContext ScenarioContext;

        protected ConfirmationPageSteps(ScenarioContext scenarioContext)
        {
            ScenarioContext = scenarioContext;
        }

        [Then(@"Total price should be correct in confirmation page")]
        public void TotalPriceCheck()
        {
            Assert.AreEqual(ScenarioContext["TotalPrice"],Text.GetText(CommonLocators.TotalPrice));
        }

        [Then(@"Selected seats must be correctly displayed in confirmation page")]
        public void SeatCheck()
        {
            Assert.IsTrue(Action.CompareLists(SeatsPageSteps.selectedSeats,
                SeatSelector.GetConfirmPageSeats(ConfirmationPageLocators.SummaryTableRows)));
        }

        [Given(@"I have pressed \[Change order] button")]
        public void ChangeOrderBtnPress()
        {
            Action.JavaScriptClick(ConfirmationPageLocators.ChangeOrderBtn);
        }

        [Then(@"Total price should be chaged")]
        public void TotalPriceChange()
        {
            Assert.AreNotEqual(ScenarioContext["TotalPrice"], Text.GetText(CommonLocators.TotalPrice));
        }

        [Then(@"Confirmation page should contain (.*) payment methods")]
        public void PaymentCheck(string p)
        {
            Assert.IsTrue(Check.CheckElementsAmount(ConfirmationPageLocators.PaymentMethods,p));
        }
    }
}
