﻿using NeotechTask.Common;
using NeotechTask.Locators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class ProfilePageSteps:StepBase
    {
        [Then(@"(.*) name should be present in profile view")]
        public void NameShouldBePresent(string userTestData)
        {
            var _user = TestData.GetTestData(userTestData);
            Assert.AreEqual(_user.Name, Action.GetElementsValueAttribValue(ProfilePageLocators.Name));
        }

        [Then(@"(.*) surname should be present in profile view")]
        public void SurnameShouldBePresent(string userTestData)
        {
            var _user = TestData.GetTestData(userTestData);
            Assert.AreEqual(_user.Surname, Action.GetElementsValueAttribValue(ProfilePageLocators.Surname));
        }

        [When(@"I press logout button")]
        public void LogoutBtnPress()
        {
            Action.ClickWaitForProgressBar(ProfilePageLocators.LogOutBtn);
        }
    }
}
