﻿using NeotechTask.Common;
using NeotechTask.Locators;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public sealed class StartPageSteps:StepBase
    {
        [Given(@"I have openeed Cinamon page")]
        public void OpenCinamonPage()
        {
            Navigation.GotoUrl(Config.CinamonUrl);
        }

        [Given(@"I have pressed start page \[Log in] button")]
        public void PressLoginButton()
        {
            Action.Click(StartPageLocators.LogInButton);
        }

        [Then(@"Start page title (.*) should be displayed")]
        public void ProfileViewCheck(string title)
        {
            Assert.IsTrue(Check.CheckWindowTitle(title));
        }
    }
}
