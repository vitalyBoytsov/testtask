﻿using NeotechTask.Common;
using NeotechTask.Locators;
using TechTalk.SpecFlow;

namespace NeotechTask.Steps
{
    [Binding]
    public class LoginPageSteps:StepBase
    {
         
        [Given(@"I have entered (.*) email address")]
        public void EnterEmail(string userTestData)
        {
            var _user = TestData.GetTestData(userTestData);
            Action.Type(LoginPageLocators.EmailInputField, _user.Email);
        }

        [Given(@"I have entered (.*) password")]
        public void EnterPassword(string userTestData)
        {
            var _user = TestData.GetTestData(userTestData);
            Action.Type(LoginPageLocators.PasswordInputField, _user.Password);
        }

        [When(@"I press log in form \[Log in] button")]
        [Given(@"I have pressed log in form \[Log in] button")]
        public void LoginButtonPress()
        {
            Action.ClickWaitForProgressBar(LoginPageLocators.LogInButton);
        }
    }
}
