﻿Feature: Tests
	This feature contains Cinamon web page
	basic funcionality tests

Background:
	Given I have openeed Cinamon page
	And I have pressed start page [Log in] button
	And I have entered Neotech_User_1 email address
	And I have entered Neotech_User_1 password
 
 Scenario Outline: Log in and check that user name is present in profile
	When I press log in form [Log in] button
	Then <User> name should be present in profile view

	Examples: 
	| User           |
	| Neotech_User_1 |

 Scenario Outline: Log out funcionality check
	And I have pressed log in form [Log in] button
	And I have checked that we are transfered to <Title> view
	When I press logout button
	Then Start page title <Start tile> should be displayed
	 
	Examples: 
	| User           | Title      | Start tile   |
	| Neotech_User_1 | Mans konts | Cinamon Riga |

 Scenario Outline: Log in and check that user surname is present in profile
	When I press log in form [Log in] button
	Then <User> surname should be present in profile view

	Examples: 
	| User           |
	| Neotech_User_1 |

 Scenario Outline: Ticket total price check in confirmation page
	And I have pressed log in form [Log in] button
	And I have selected <Cinema> cinema
	And I have selected random movie that has session after <Days> days
	And I have selected random movie date
	And I have pressed [Buy now] button
	And I have selected <Amount> of Friends tickets
	And I have saved total price
	And I have pressed [Next] button in ticket page
	When I press [Select] button in seats page
	Then Total price should be correct in confirmation page

	Examples: 
	| User           | Cinema            | Amount | Days |
	| Neotech_User_1 | Cinamon Alfa Riga | 2      | 2    |

 Scenario Outline: Payment methods amount check in confirmation page
	And I have pressed log in form [Log in] button
	And I have selected <Cinema> cinema
	And I have selected random movie that has session after <Days> days
	And I have selected random movie date
	And I have pressed [Buy now] button
	And I have selected <Amount> of Friends tickets
	And I have pressed [Next] button in ticket page
	When I press [Select] button in seats page
	Then Confirmation page should contain <Payments> payment methods

	Examples: 
	| User           | Cinema            | Amount | Days | Payments |
	| Neotech_User_1 | Cinamon Alfa Riga | 1      | 2    | 5        |

 Scenario Outline: Seats check in confirmation page
	And I have pressed log in form [Log in] button
	And I have selected <Cinema> cinema
	And I have selected random movie that has session after <Days> days
	And I have selected random movie date
	And I have pressed [Buy now] button
	And I have selected <Amount> of Friends tickets
	And I have pressed [Next] button in ticket page
	And I have selected random seat from 5 row
	And I have selected random seat from 6 row
	And I have saved selected seats in seats page
	When I press [Select] button in seats page
	Then Selected seats must be correctly displayed in confirmation page

	Examples: 
	| User           | Cinema            | Amount | Days |
	| Neotech_User_1 | Cinamon Alfa Riga | 2      | 2    |

 Scenario Outline: Change order funcionality check
	And I have pressed log in form [Log in] button
	And I have selected <Cinema> cinema
	And I have selected random movie that has session after <Days> days
	And I have selected random movie date
	And I have pressed [Buy now] button
	And I have selected <Amount> of Friends tickets
	And I have pressed [Next] button in ticket page
	And I have pressed [Select] button in seats page
	And I have saved total price
	And I have pressed [Change order] button
	And I have selected <Changed amount> of Friends tickets
	And I have pressed [Next] button in ticket page
	When I press [Select] button in seats page
	Then Total price should be chaged
	 
	Examples: 
	| User           | Cinema            | Amount | Days | Changed amount |
	| Neotech_User_1 | Cinamon Alfa Riga | 2      | 2    | 1              |

 Scenario Outline: Cupon validation error check
	And I have pressed log in form [Log in] button
	And I have selected <Cinema> cinema
	And I have selected random movie that has session after <Days> days
	And I have selected random movie date
	And I have pressed [Buy now] button
	And I have entered random coupon id
	When I press [Submit coupon] button
	Then Coupon validtion error should be displayed

	Examples: 
	| User           | Cinema            | Amount | Days |
	| Neotech_User_1 | Cinamon Alfa Riga | 2      | 2    |

