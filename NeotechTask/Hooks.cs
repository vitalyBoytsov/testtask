﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NeotechTask.Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Tracing;

namespace NeotechTask
{
    [Binding]
    public  class Hooks
    {
        public ScenarioContext ScenarioContext;
        private static IWebDriver Driver;


        public Hooks(ScenarioContext scenarioContext)
        {
            ScenarioContext = scenarioContext;
        }


        [BeforeScenario]
        public void BeforeScenario()
        {
            Driver = GetWebDriver();
            StateHolder.IntitializeDriver(Driver);
            Driver.Manage().Window.Maximize();


        }

        [AfterScenario]
        public void AfterScenario()
        {
            if (ScenarioContext.TestError != null) TakeScreenshot(Driver);
            if (Driver != null) Driver.Quit();
            StateHolder.DiscardDriver();
        }


        public static IWebDriver GetWebDriver()
        {
            switch (Config.Browser)
            {
                case "Chrome":
                    return new ChromeDriver();
                case "PhantomJs":
                    return new PhantomJSDriver();
            }
            throw new Exception("Incorrect browser name in App.config");
        }

        private void TakeScreenshot(IWebDriver driver)
        {
            try
            {
                var fileNameBase = ScenarioContext.ScenarioInfo.Title.ToIdentifier();
                var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+@"\Screenshots";
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                var takesScreenshot = driver as ITakesScreenshot;
                if (takesScreenshot == null) return;
                var screenshot = takesScreenshot.GetScreenshot();
                var screenshotFilePath = Path.Combine(dir, fileNameBase + ".png");
                screenshot.SaveAsFile(screenshotFilePath, ScreenshotImageFormat.Png);
                Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath.Replace(" ", "%20")));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
        }
    }
}
