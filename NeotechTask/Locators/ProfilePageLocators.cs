﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class ProfilePageLocators
    {
        public static readonly By Name = By.CssSelector("[name='firstname']");
        public static readonly By Surname = By.CssSelector("[name='lastname']");
        public static readonly By LogOutBtn = By.CssSelector("[class='logout']");
    }
}
