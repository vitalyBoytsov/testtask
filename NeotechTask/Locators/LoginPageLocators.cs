﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class LoginPageLocators
    {
        public static readonly By EmailInputField = By.CssSelector("[name*='login_form'] input[type='email']");
        public static readonly By PasswordInputField = By.CssSelector("[name*='login_form'] input[type='password']");
        public static readonly By LogInButton = By.CssSelector("[name*='login_form'] [type='submit']");
    }
}
