﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class TicketPageLocators
    {
        public static readonly By TicketIncreaseBtn = By.CssSelector("[id='ticket_form'] tbody input[value='+']");
        public static readonly By TicketDecreaseBtn = By.CssSelector("[id='ticket_form'] tbody input[value='-']");
        public static readonly By FriendsTicketIcrease =
            By.CssSelector("input[name='ticket[0003][price_in_cents]'] + input[value='+']");
        public static readonly By NextBtn = By.CssSelector("input[id='next-step']");
        public static readonly By CouponInput = By.CssSelector("[class='voucher'] input");
        public static readonly By Error = By.CssSelector("[class='error']");
        public static readonly By SubmitCoupon = By.CssSelector("[class='small secondary validate']");
    }
}
