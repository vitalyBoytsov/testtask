﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class SeatsPageLocators
    {
        public static readonly By SelectBtn = By.CssSelector("input[name='submit']");
        public static readonly By Seats = By.CssSelector("[class*='pick-tickets'] g");
        public static readonly By SelectedSeats = By.CssSelector("[class*='picked-summary'] li");
    }
}
