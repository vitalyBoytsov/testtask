﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class ConfirmationPageLocators
    {
        public static readonly By SummaryTableRows = By.CssSelector("[class*='summary-table'] tbody tr");
        public static readonly By ChangeOrderBtn = By.CssSelector("[class='button change-btn light-btn']");
        public static readonly By PaymentMethods = By.CssSelector("[class='payment-methods'] li");
    }
}
