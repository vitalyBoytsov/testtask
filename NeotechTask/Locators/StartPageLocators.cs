﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class StartPageLocators
    {
        public static readonly By LogInButton = By.CssSelector(".login-area .username");
    }
}
