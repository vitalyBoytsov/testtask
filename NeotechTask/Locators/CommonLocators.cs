﻿using OpenQA.Selenium;

namespace NeotechTask.Locators
{
    public class CommonLocators
    {
        public static readonly By ProgressBar = By.CssSelector("[id='post-10356'] script");
        public static readonly By QuickToolbar = By.CssSelector("[id='quick-booking__container']");
        public static readonly By QuickCinemaDrpdwn = By.CssSelector("select[id='quick-booking__cinema']");
        public static readonly By QuickFilmDrpdwn = By.CssSelector("select[id='quick-booking__film']");
        public static readonly By QuickSessionDrpdwn = By.CssSelector("select[id='quick-booking__session']");
        public static readonly By SessionTimes = By.CssSelector("select[id='quick-booking__session'] option");
        public static readonly By Movies = By.CssSelector("select[id='quick-booking__film'] option");
        public static readonly By QuickBuyButton = By.CssSelector("[id='quick-booking__action']");
        public static readonly By TotalPrice = By.CssSelector("[class='total']");
    }
}
