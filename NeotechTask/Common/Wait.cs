﻿using System;
using NeotechTask.Locators;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace NeotechTask.Common
{
    public class Wait
    {
        private readonly IWebDriver _driver;

        public Wait(IWebDriver driver)
        {
            _driver = driver;
        }

        public void WaitForPageToLoad()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(Config.PageLoadWaitTime))
                {
                    Message = "page not loaded"
                }
                .Until(
                    d =>
                        ((IJavaScriptExecutor)_driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void WaitUntilElementIsVisible(By by)
        {
            var seconds = TimeSpan.FromSeconds(Config.ExplicitWaitTime);
            var wait = new WebDriverWait(_driver, seconds);
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(by));
            }
            catch (WebDriverTimeoutException)
            {
                throw new Exception(string.Format("Element not found with by->{0}", by));
            }
            catch (StaleElementReferenceException)
            {
                throw new Exception(string.Format("Element not found with by->{0}", by));
            }
            catch (NoSuchElementException)
            {
                throw new Exception(string.Format("Element not found with by->{0}", by));
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Element not found with by->{0}", by));
            }
        }

        public void WaitUntilElementIsClickable(By by)
        {
            var seconds = TimeSpan.FromSeconds(Config.ExplicitWaitTime);
            var wait = new WebDriverWait(_driver, seconds);
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(by));
            }
            catch
            {
                throw new Exception(string.Format("Element is not clickable ->{0}", by));
            }
        }

        public void WaitForProgressBarHide()
        {
            
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(Config.ProgressBarDisappearWaitTime));
            TryToWaitUntilElementIsVisible();
            try
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(CommonLocators.ProgressBar));
            }
            catch
            {
                throw new Exception("Progress bar should not be visible all the time, cannot load page");
            }
        }

        public void WaitUntilElementCountIncreased(By by, int count)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(Config.ExplicitWaitTime)).Until(d =>
                d.FindElements(by).Count > count);
        }

        private void TryToWaitUntilElementIsVisible()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(Config.ProgressBarAppearWaitTime));

            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(CommonLocators.ProgressBar));
            }
            catch
            {
               
            }
        }

    }
}
