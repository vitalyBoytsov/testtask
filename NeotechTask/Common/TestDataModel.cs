﻿using System;

namespace NeotechTask.Common
{
    public class TestDataModel
    {
        public string TestDataName { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }

        public TestDataModel(string testDataName, string name, string surname, string password, string email)
        {
            TestDataName = testDataName;
            Name = name;
            Surname = surname;
            Password = password;
            Email = email;
        }
    }
}
