﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeotechTask.Locators;
using OpenQA.Selenium;

namespace NeotechTask.Common
{
    public class SeatSelector
    {
        private readonly IWebDriver _driver;

        public SeatSelector(IWebDriver driver)
        {
            _driver = driver;
        }


        public void SelectRandomRowSeats(By by, string row)
        {  
            new Action(_driver).HideElement(CommonLocators.QuickToolbar);
            var rowList = GetSpecificRowSeatlist(by, row);
            if (!CheckRowSeatAvailability(rowList)) throw new Exception("All seats in row are reserved");
            new Action(_driver).NavigateAndClick(Action.GetRandomElFromList(GetFreeRowSeats(rowList)).FindElement(By.CssSelector("rect")));
        }

        private bool CheckRowSeatAvailability(List<IWebElement> rows)
        {
            if (GetFreeRowSeats(rows).Count == 0) return false;
            return true;
        }

        private List<IWebElement> GetFreeRowSeats(List<IWebElement> allRowSeats)
        {
            List<IWebElement> freeSeats = new List<IWebElement>();
            allRowSeats.ForEach(i => { if (!i.GetAttribute("class").Contains("broken")) freeSeats.Add(i); });
            return freeSeats;
        }

        private List<IWebElement> GetSpecificRowSeatlist(By by, string rowNum)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            List<IWebElement> result = new List<IWebElement>();
            _driver.FindElements(by).ToList().ForEach(i => {if (i.GetAttribute("seatmeta").Contains(String.Format("\"RowId\":\"{0}\"", rowNum)))
                    result.Add(i);});
            return result;
        }

        public List<string> GetConfirmPageSeats(By summaryTableRows)
        {
            new Wait(_driver).WaitUntilElementIsVisible(summaryTableRows);
            var rows = _driver.FindElements(summaryTableRows);
            List<string> result = new List<string>();
            int index=0;
            foreach (var row in rows)
            {
                var colums = row.FindElements(By.CssSelector("td"));
                colums.ToList().ForEach(colum => {if (colum.GetAttribute("class").Contains("film-stamp"))index =
                    rows.IndexOf(row)+1;
                } );
            }
            var count = rows.Count - index;
            rows.ToList().GetRange(index, count).ForEach(i => result.Add(i.Text.ToLower().Replace(" ","").Replace("-","").Replace(",","")));
            return result;
        }

        public List<string> GetSeatPageSeats(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            List<string> result = new List<string>();
            _driver.FindElements(by).ToList().ForEach(i => result.Add(i.Text.ToLower().Replace(" ", "").Replace(",", "")));
            return result;
        }

    }
}
