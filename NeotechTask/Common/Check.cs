﻿using OpenQA.Selenium;

namespace NeotechTask.Common
{
    public class Check
    {
        private readonly IWebDriver _driver;

        public Check(IWebDriver driver)
        {
            _driver = driver;
        }

        public bool CheckWindowTitle(string title)
        {
            if (_driver.Title.Contains(title)) return true;
            return false;
        }

        public bool CheckElementsAmount(By by,string amount)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            if (_driver.FindElements(by).Count.ToString() == amount) return true;
            return false;
        }

    }
    
}
