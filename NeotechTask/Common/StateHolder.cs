﻿using System;
using OpenQA.Selenium;

namespace NeotechTask.Common
{
    public class StateHolder
    {
        private static IWebDriver Driver;

        public static T CreateObject<T>()
        {
            T obj = (T)Activator.CreateInstance(typeof(T), Driver);
            return obj;
        }

        public static IWebDriver IntitializeDriver(IWebDriver _driver)
        {
            if (Driver == null)
            {
                Driver = _driver;
                return Driver;
            }
            throw new Exception("Driver already setted up");
        }

        public static void DiscardDriver()
        {
            Driver = null;
        }
    }
}
