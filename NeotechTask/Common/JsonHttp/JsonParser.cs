﻿using Newtonsoft.Json;

namespace NeotechTask.Common.JsonHttp
{
    public class JsonParser
    {
        public static T JsonCreator<T>(string raw)
        {
            return JsonConvert.DeserializeObject<T>(raw);
        }
    }
}
