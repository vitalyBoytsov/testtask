﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace NeotechTask.Common.JsonHttp
{
    public class HttpRunner
    {

        private static async Task<string> httpPost(string url, Dictionary<string, string> values)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var response = await client.PostAsync(url, new FormUrlEncodedContent(values));
                    return await response.Content.ReadAsStringAsync();
                }

                catch (HttpRequestException e)
                {
                    throw new Exception("Connection to " + url + " is failed with exception: " + e.Message);
                }
            }
        }

        private static async Task<KeyValuePair<string, string>> httpPostSessionGet(string id)
            {
                var values = new Dictionary<string, string>
                {
                    {"action", "vista_film_sessions"},
                    {"film_id", id},
                    {"lang", "lv"}
                };
                var response = await httpPost(Config.SessionAPI, values);
                return new KeyValuePair<string, string>(id, response);
            }

        public static async Task<KeyValuePair<string, string>[]> rawSessionGetter(List<string> ids)
            {
                var result = await Task.WhenAll(ids.Select(id => httpPostSessionGet(id)));
                return result;
            }

        
    }
}
