﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace NeotechTask.Common
{
    public class Action
    {
         
        private readonly IWebDriver _driver;

        public Action(IWebDriver driver)
        {
            _driver = driver;
        }

        public void Click(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            new Wait(_driver).WaitUntilElementIsClickable(by);
            _driver.FindElement(by).Click();
        }

        public void NavigateAndClick(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            new Wait(_driver).WaitUntilElementIsClickable(by);
            var el = _driver.FindElement(by);
            Actions action = new Actions(_driver);
            action.MoveToElement(el).Click().Build().Perform();
        }

        public void NavigateAndClick(IWebElement el)
        {  
            Actions action = new Actions(_driver);
            action.MoveByOffset(el.Location.X,el.Location.Y).MoveToElement(el).Click().Build().Perform();
        }

        public void JavaScriptClick(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var element = _driver.FindElement(by);
            var executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public void JavaScriptClick(IWebElement element)
        {
            var executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public void ClickWaitForProgressBar(By by)
        {
            Click(by);
            new Wait(_driver).WaitForProgressBarHide();
        }

        public void Type(By by, string text)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            _driver.FindElement(by).SendKeys(text);
            Console.WriteLine("Entered: " + text);
        }

        public void TypeRandomValue(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var text = new Random().Next(0,10).ToString();
            _driver.FindElement(by).SendKeys(text);
            Console.WriteLine("Entered: " + text);
        }

        public string GetElementsValueAttribValue(By by)
        {
            return GetElementsAtributeValue(by, "value");
        }

        public void SelectValueFromDropDown(By by, string valueText)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var element = _driver.FindElement(by);
            var selectElement = new SelectElement(element);
            if (selectElement.SelectedOption.Text == valueText) return;
            selectElement.SelectByText(valueText);
          
        }

        public void SelectValueFromDropDownByValue(By by, string value)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var element = _driver.FindElement(by);
            var selectElement = new SelectElement(element);
            selectElement.SelectByValue(value);

        }

        public void SelectRandomValueFromDrpdown(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var element = _driver.FindElement(by);
            var selectElement = new SelectElement(element);
            var index = new Random().Next(0, selectElement.Options.Count - 1);
            selectElement.SelectByIndex(index);
        }

        public void MultipleClickElement(By by, string clicks)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var el = _driver.FindElement(by);
            var intClicks = Int32.Parse(clicks);
            do
            {
                JavaScriptClick(el);
                intClicks--;
            } while (intClicks != 0);
        }

        public string GetElementsValue(IWebElement el)
        {
            try
            {
                return el.GetAttribute("value");
            }
            catch (NullReferenceException)
            {
                throw new Exception("Element is not returned");
            }
        }

        public static T GetRandomElFromList<T>(List<T> list)
        {
            var randIndex = new Random().Next(0, list.Count - 1);
            return list[randIndex];
        }

        public bool CompareLists<T>(List<T> list1, List<T> list2)
        {
            var result = true;
            foreach (var i in list1)
            {
                if (!list2.Contains(i)) result = false;
            }
            return result;
        }

        public void HideElement(By by)
        {
            var element = _driver.FindElement(by);
            var executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("arguments[0].style.visibility = 'hidden'", element);
        }

        private IWebElement PickRandomEl(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            var elements = _driver.FindElements(by);
            return GetRandomElFromROCollection(elements);
        }
  
        private T GetRandomElFromROCollection<T>(ReadOnlyCollection<T> list)
        {
            var randIndex = new Random().Next(0, list.Count - 1);
            return list[randIndex];
        }

        private string GetElementsAtributeValue(By by, string atribute)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            return _driver.FindElement(by).GetAttribute(atribute);
        }

    }
}
