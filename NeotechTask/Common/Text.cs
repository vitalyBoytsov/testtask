﻿using OpenQA.Selenium;

namespace NeotechTask.Common
{
    public class Text
    {
        private readonly IWebDriver _driver;

        public Text(IWebDriver driver)
        {
            _driver = driver;
        }

        public string GetText(By by)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            return _driver.FindElement(by).Text;
        }

        
    }
}
