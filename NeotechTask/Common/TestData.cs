﻿using System.Collections.Generic;
using System.Linq;


namespace NeotechTask.Common
{
    public class TestData
    {
        public const string CouponError = "Nederīgs kupona numura formāts.";

        private static readonly List<TestDataModel> _testDaraList = new List<TestDataModel>
        {
            new TestDataModel("Neotech_User_1", "Neo", "Tech", "test123", "neotechtest@mailinator.com" )
        };

        public static TestDataModel GetTestData(string name)
        {
            return _testDaraList.First(item => item.TestDataName.ToLower().Equals(name.ToLower()));
        }
    }
}
