﻿using System;

namespace NeotechTask.Common
{
    public class StepBase
    {
        public Action Action => StateHolder.CreateObject<Action>();
        public Check Check => StateHolder.CreateObject<Check>();
        public Navigation Navigation => StateHolder.CreateObject<Navigation>();
        public Text Text => StateHolder.CreateObject<Text>();
        public Wait Wait => StateHolder.CreateObject<Wait>();
        public MovieSessions MovieSessions => StateHolder.CreateObject<MovieSessions>();
        public SeatSelector SeatSelector => StateHolder.CreateObject<SeatSelector>();
    }
}
