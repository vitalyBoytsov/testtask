﻿using System.Collections.Generic;

namespace NeotechTask.Common.JsonHttp
{
    public class Model
    {
        public class DateTime
        {
            public string id { get; set; }
            public string time { get; set; }
        }

        public class Sessions
        {
            public IList<DateTime> sessions { get; set; }
        }

        public class SessionGetResponse
        {
            public bool success { get; set; }
            public Sessions data { get; set; }
        }
    }
}
