﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeotechTask.Common.JsonHttp;
using OpenQA.Selenium;
using TechTalk.SpecRun.Common.Helper;

namespace NeotechTask.Common
{
    public class MovieSessions
    {
        private readonly IWebDriver _driver;

        public MovieSessions(IWebDriver driver)
        {
            _driver = driver;
        }
        
        public List<MovieDateTimeObject> GetSpecificDateMovies(By by, string days)
        {
            var incr = Int32.Parse(days);
            if (incr < 0) throw new Exception("Movie date can not be less thah todays date");
            var movieTimeList = GetMovieTimeList(by);
            var newList = new List<MovieDateTimeObject>();
            bool dateResult = true;
            string date = null;
            foreach (var item in movieTimeList)
            {
                foreach (var i in item.Value.data.sessions)
                {
                    if (i.id == "" && i.time.Contains(" " + GetSpecificFutureDateDay(incr) + ". "))
                    {
                        dateResult = true;
                        date = i.time;
                        continue;
                    }

                    if (i.id == "" && !i.time.Contains(" " + GetSpecificFutureDateDay(incr) + ". "))
                    {
                        dateResult = false;
                        continue;
                    }

                    if (dateResult) newList.Add(new MovieDateTimeObject(item.Key, date, i.time));
                }
            }

            if(newList.Count == 0)throw new Exception("There is no movies available for a date: "+GetSpecificFutureDate(incr));
            return newList;
        }

        public MovieDateTimeObject GetSpecificDateRandomMovie(By by, string incr)
        {
            return Action.GetRandomElFromList<MovieDateTimeObject>(GetSpecificDateMovies(by, incr));
        }

        public IWebElement GetSessionIWebEl(By by, MovieDateTimeObject obj)
        {
            new Wait(_driver).WaitUntilElementIsVisible(by);
            new Wait(_driver).WaitUntilElementCountIncreased(by, 1);
            var options = _driver.FindElements(by);
            bool result = false;
            IWebElement element = null;
            foreach (var option in options)
            {
                if (!option.Enabled && option.Text == obj.Date)
                {
                    result = true;
                    continue;
                }
                if (!option.Enabled && option.Text != obj.Date)
                {
                    result = false;
                    continue;
                }

                if (result && option.Text == obj.Time)
                {
                    element = option;
                    break;
                }

            }

            return element;
        }

        private static List<KeyValuePair<string, Model.SessionGetResponse>> GetMovieSessions(List<string> ids)
        {
            var result = new List<KeyValuePair<string, Model.SessionGetResponse>>();
            var response = HttpRunner.rawSessionGetter(ids).GetAwaiter().GetResult();
            foreach (var i in response)
            {
                if (i.Value.IsNullOrEmpty()) continue;
                result.Add(new KeyValuePair<string, Model.SessionGetResponse>(i.Key, JsonParser.JsonCreator<Model.SessionGetResponse>(i.Value)));
            }
            if (result == null) throw new Exception("No session data returned from API");
            return result;
        }

        private List<KeyValuePair<string, Model.SessionGetResponse>> GetMovieTimeList(By by)
        {
            var movieIds = new List<string>();
            new Wait(_driver).WaitUntilElementIsVisible(by);
            _driver.FindElements(by).ToList().ForEach(i =>
                { if (i.GetAttribute("value") != "") movieIds.Add(i.GetAttribute("value")); });
            return GetMovieSessions(movieIds);
        }

        private string GetSpecificFutureDateDay(int i)
        {
            return GetCurrentDate().AddDays(i).Day.ToString();
        }

        private string GetSpecificFutureDate(int i)
        {
            return GetCurrentDate().AddDays(i).ToShortDateString();
        }

        private string GetRandomFutureDateDay()
        {
            return GetCurrentDate().AddDays(new Random().Next(1, 5)).Day.ToString();
        }

        private string GetCurrentDateDay()
        {
            return GetCurrentDate().Day.ToString();
        }

        private DateTime GetCurrentDate()
        {
            return DateTime.Today;
        }

        private List<string> GetListOfDictValues(List<MovieDateTimeObject> dates, string day)
        {
            var list = new List<string>();
            foreach (var item in dates)
            {
                if (item.Date.Contains(day)) list.Add(item.Time);
            }
            return list;
        }
    }
}
