﻿using System;

namespace NeotechTask.Common
{
    public class MovieDateTimeObject
    {
        public string Movie { get; }
        public string Date { get; }
        public string Time { get; }

        public MovieDateTimeObject(string movie, string date, string time)
        {
            Movie = movie;
            Date = date;
            Time = time;
        }
    }
}
