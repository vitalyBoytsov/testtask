﻿using System;
using OpenQA.Selenium;

namespace NeotechTask.Common
{
    public class Navigation
    {
        private readonly IWebDriver _driver;

        public Navigation(IWebDriver driver)
        {
            _driver = driver;
        }

        public void GotoUrl(string url)
        {
            _driver.Url = url;
            Console.WriteLine("Url name: " + url);
            new Wait(_driver).WaitForPageToLoad();
        }
    }
}
